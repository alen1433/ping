{
    "id": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_Handeler",
    "eventList": [
        {
            "id": "5f31f548-108f-4a4a-882e-3cc7b97bce45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "c5bbd950-86e0-4ba5-91c2-0078d66895e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "854f48c3-39e7-4f98-a4c6-f55da8e2a73d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "9ed8da3b-735c-40bc-805f-1eff51ab67f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "840ff4a7-4c00-4c12-97a5-fc9b9e184de4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "cb77f894-de3b-4e86-ae5e-10df8e183337",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "df84584e-7dc0-4130-afe4-9afe7746a662",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "5497bc33-ab5a-4d20-8343-ec0788a9765c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "d51a246c-28e7-46bb-bdfe-8ab9f718d625",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        },
        {
            "id": "64acfad4-81a5-403e-9c09-8a7ad3eb492b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "04f21bd8-f9d2-43af-a1c8-1613d6fbbe5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}