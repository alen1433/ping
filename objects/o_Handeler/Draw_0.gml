/// @description Drawing Of Text

draw_set_color(c_gray);
draw_text_transformed(room_width/2-100, 10, global.player1score, 10, 10, 0);
draw_text_transformed(room_width/2+100, 10, global.player2score, 10, 10, 0);

if(global.state = gameState.gameStart){
	if(global.theme == 1){
		draw_set_color(c_black);
	}else{
		draw_set_color(c_white);
	}
	draw_text_transformed(room_width/2-200, room_height/2-100, "PRESS 'W'\nTO START", 5, 5, 0);
	if(global.player1ready){
		draw_text_transformed(room_width/2-200, room_height/2+200, "READY", 5, 5, 0);
	}
	if(!global.singlePlayer){
		draw_text_transformed(room_width/2+200, room_height/2-100, "PRESS 'up'\nTO START", 5, 5, 0);
		if(global.player2ready){
			draw_text_transformed(room_width/2+200, room_height/2+200, "READY", 5, 5, 0);
		}
	}
}else if(global.state = gameState.gameEnd){
	if(global.player1score == global.scoreLimit){
		draw_set_color(c_green);
		draw_text_transformed(room_width/2-200, room_height/2-100, "WINNER", 8, 8, 0);
		draw_set_color(c_red);
		draw_text_transformed(room_width/2+200, room_height/2-100, "LOSER", 8, 8, 0);
	}else{
		draw_set_color(c_green);
		draw_text_transformed(room_width/2+200, room_height/2-100, "WINNER", 8, 8, 0);
		draw_set_color(c_red);
		draw_text_transformed(room_width/2-200, room_height/2-100, "LOSER", 8, 8, 0);
	}
	if(continueGame){
		draw_set_color(c_red);
	}else{
		if(global.theme == 1){
			draw_set_color(c_black);
		}else{
			draw_set_color(c_white);
		}
	}
	draw_text_transformed(room_width/2, room_height-200, "AGAIN", 5, 5, 0);
	if(continueGame){
		if(global.theme == 1){
			draw_set_color(c_black);
		}else{
			draw_set_color(c_white);
		}
	}else{
		draw_set_color(c_red);
	}
	draw_text_transformed(room_width/2, room_height-100, "QUIT", 5, 5, 0);
	
}