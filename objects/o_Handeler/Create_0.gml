/// @description Global variable holder

global.paddleSpeed = 10.0;
global.player1ready = false;
global.player2ready = false;
global.player1score = 0;
global.player2score = 0;
global.ballScored = false;

draw_set_font(FontBasic);
draw_set_halign(fa_center);


firstRound = true;
continueGame = true;

enum gameState {
	gameStart,
	gameRunning,
	gameScore,
	gameEnd
}

var lay_id = layer_get_id("Background");
var back_id = layer_background_get_id(lay_id);

switch(global.theme){
	case 0: layer_background_blend(back_id, c_black);
			break;
	case 1: layer_background_blend(back_id, c_white);
			break;
	case 2: layer_background_blend(back_id, c_white);
			layer_background_sprite(back_id, BgSoccer);
			break;
	case 3: layer_background_blend(back_id, c_white);
			layer_background_sprite(back_id, BgBeach);
			break;
}

global.state = gameState.gameStart;