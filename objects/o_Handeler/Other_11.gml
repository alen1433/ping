/// @description The game is running

if(global.ballScored){
	if(global.player1score == global.scoreLimit || global.player2score == global.scoreLimit){
		global.ballScored = false;
		global.state = gameState.gameEnd;
	}else{
		global.ballScored = false;
		alarm[1] = room_speed * 2;
	}
}
