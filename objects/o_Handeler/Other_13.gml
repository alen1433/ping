/// @description The game has ended
if(continueGame && (keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down))){
	continueGame = false;	
}else if(continueGame && (keyboard_check_pressed(vk_enter))){
	room_restart()
}
if(!continueGame && (keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up))){
	continueGame = true;	
}else if(!continueGame && (keyboard_check_pressed(vk_enter))){
	room_persistent = false;
	room_goto(0);
}