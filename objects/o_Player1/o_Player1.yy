{
    "id": "74f86429-7746-429f-a299-96980996479f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_Player1",
    "eventList": [
        {
            "id": "74e1ba33-9d2c-4218-bdcd-2c8e1c2b4b8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74f86429-7746-429f-a299-96980996479f"
        },
        {
            "id": "08b456ee-1072-4224-8047-7a58002e324f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74f86429-7746-429f-a299-96980996479f"
        },
        {
            "id": "8e296772-df4e-4427-9cd4-6b640a4a7b3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "74f86429-7746-429f-a299-96980996479f"
        },
        {
            "id": "08a33cac-782c-4d82-a5fe-ea9971010909",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "74f86429-7746-429f-a299-96980996479f"
        },
        {
            "id": "346f42f9-d0b1-443b-8160-e0f40c6d90d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "74f86429-7746-429f-a299-96980996479f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b51dc4cb-58fc-442b-97f1-bee3864b11e9",
    "visible": true
}