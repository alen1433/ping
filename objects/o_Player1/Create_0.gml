/// @description The Initalization script
image_xscale = 1.5; image_yscale = 1.5;

switch(global.theme){
	case 0: sprite_index = PaddleNormal; break;
	case 1: sprite_index = PaddleInverted; break;
	case 2: sprite_index = PaddleSoccer1; break;
	case 3: sprite_index = PaddleBeach1; break;
}