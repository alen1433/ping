/// @description The Game Is Running

//set direction of movement
moveDir = 0 - keyboard_check(ord("W")) + keyboard_check(ord("S"));

//check for collision with roof and floor
if(y + global.paddleSpeed * moveDir < 1 + sprite_height/2){
	while(y > 1 + sprite_height/2){
		y -= 1;	
	}
	moveDir = 0;
}else if(y + global.paddleSpeed * moveDir > room_height - 1 - sprite_height/2){
	while(y < room_height - 1 - sprite_height/2){
		y += 1;	
	}
	moveDir = 0;
}

//finaly move paddle
y += global.paddleSpeed * moveDir;