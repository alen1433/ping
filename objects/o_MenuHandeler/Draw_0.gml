/// @description Draw Everthing

if(currentState == 0){
	draw_set_color(c_white);
	draw_text_transformed(room_width/2, room_height/2 - 400, "PING", 20, 20, 0);
	if(curserLoc == 0){
		draw_set_color(c_red);
	}else{
		draw_set_color(c_white);
	}
	draw_text_transformed(room_width/2, room_height/2, "1 PLAYER", 5, 5, 0);
	if(curserLoc == 1){
		draw_set_color(c_red);
	}else{
		draw_set_color(c_white);
	}
	draw_text_transformed(room_width/2, room_height/2 + 100, "2 PLAYER", 5, 5, 0);
	if(curserLoc == 2){
		draw_set_color(c_red);
	}else{
		draw_set_color(c_white);
	}
	draw_text_transformed(room_width/2, room_height/2 + 200, "QUIT", 5, 5, 0);
}else{
	if(global.singlePlayer){
		draw_set_color(c_white);
		draw_text_transformed(room_width/2, room_height/2 - 300, "Move the paddle\nwith 'W' and 'S'", 3, 3, 0);
	}else{
		draw_set_color(c_white);
		draw_text_transformed(room_width/2, room_height/2 - 350, "Player 1 moves the paddle\nwith 'W' and 'S'", 3, 3, 0);
		draw_text_transformed(room_width/2, room_height/2 - 230, "Player 2 moves the paddle\nwith 'up' and 'down'", 3, 3, 0);
	}
	draw_text_transformed(room_width/2, room_height/2 - 100, "SCORE LIMIT", 5, 5, 0);
	if(curserLoc == 0){
		draw_set_color(c_red);
	}else{
		draw_set_color(c_white);
	}
	draw_text_transformed(room_width/2, room_height/2 - 30, "< " + string(global.scoreLimit) + " >", 4, 4, 0);
	draw_set_color(c_white);
	draw_text_transformed(room_width/2, room_height/2 + 50, "THEME", 5, 5, 0);
	if(curserLoc == 1){
		draw_set_color(c_red);
	}else{
		draw_set_color(c_white);
	}
	draw_text_transformed(room_width/2, room_height/2 + 120, "< " + themeNames[global.theme] + " >", 4, 4, 0);
	if(curserLoc == 2){
		draw_set_color(c_red);
	}else{
		draw_set_color(c_white);
	}
	draw_text_transformed(room_width/2, room_height/2 + 200, "START", 5, 5, 0);
	if(curserLoc == 3){
		draw_set_color(c_red);
	}else{
		draw_set_color(c_white);
	}
	draw_text_transformed(room_width/2, room_height/2 + 300, "BACK", 5, 5, 0);
}