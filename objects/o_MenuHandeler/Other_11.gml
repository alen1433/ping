/// @description Pre-Game Settings
if(curserLoc == 0 && (keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down))){
	curserLoc++;	
}else if(curserLoc == 1 && (keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down))){
	curserLoc++;
}else if(curserLoc == 1 && (keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up))){
	curserLoc--;
}else if(curserLoc == 2 && (keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down))){
	curserLoc++;
}else if(curserLoc == 2 && (keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up))){
	curserLoc--;
}else if(curserLoc == 3 && (keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up))){
	curserLoc--;
}else if(curserLoc == 0 && (keyboard_check_pressed(ord("A")) || keyboard_check_pressed(vk_left))){
	if(global.scoreLimit > 1){
		global.scoreLimit --;
	}
}else if(curserLoc == 0 && (keyboard_check_pressed(ord("D")) || keyboard_check_pressed(vk_right))){
	if(global.scoreLimit < 10){
		global.scoreLimit ++;
	}
}else if(curserLoc == 1 && (keyboard_check_pressed(ord("A")) || keyboard_check_pressed(vk_left))){
	if(global.theme > 0){
		global.theme --;
	}
}else if(curserLoc == 1 && (keyboard_check_pressed(ord("D")) || keyboard_check_pressed(vk_right))){
	if(global.theme < 3){
		global.theme ++;
	}
}else if(curserLoc == 2 && keyboard_check_pressed(vk_enter)){
	room_goto(1);
}else if((curserLoc == 3 && keyboard_check_pressed(vk_enter)) || keyboard_check_pressed(vk_escape)){
	currentState = menuState.main;
	curserLoc = 0;
}