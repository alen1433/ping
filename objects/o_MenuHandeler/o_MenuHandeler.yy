{
    "id": "af3be175-98d1-4321-b415-4e310ddf7fac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_MenuHandeler",
    "eventList": [
        {
            "id": "5ee84dc4-be7d-4d21-8107-cf611555864b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "af3be175-98d1-4321-b415-4e310ddf7fac"
        },
        {
            "id": "f6d9d881-4753-4e56-a43a-507979f0e2a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "af3be175-98d1-4321-b415-4e310ddf7fac"
        },
        {
            "id": "d542e901-99e3-4847-91c2-40a83b90fdb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "af3be175-98d1-4321-b415-4e310ddf7fac"
        },
        {
            "id": "e19d0dcd-c18d-43ab-9ad7-6b036a3783ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "af3be175-98d1-4321-b415-4e310ddf7fac"
        },
        {
            "id": "68c26a33-e155-431c-b76a-eff6340b9783",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "af3be175-98d1-4321-b415-4e310ddf7fac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}