/// @description Initalization

enum menuState {
	main,
	preGame
}

global.theme = 0;
global.singlePlayer = true;
global.scoreLimit = 1;

currentState = menuState.main;
curserLoc = 0;
themeNames = ["CLASSIC", "INVERTED", "SOCCER", "BEACH"];

draw_set_font(FontBasic);
draw_set_halign(fa_center);