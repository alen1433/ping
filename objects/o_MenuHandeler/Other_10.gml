/// @description Main Menu
if(curserLoc == 0 && (keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down))){
	curserLoc++;	
}else if(curserLoc == 1 && (keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down))){
	curserLoc++;
}else if(curserLoc == 1 && (keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up))){
	curserLoc--;
}else if(curserLoc == 2 && (keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up))){
	curserLoc--;
}else if(curserLoc == 0 && keyboard_check_pressed(vk_enter)){
	global.singlePlayer = true;
	currentState = menuState.preGame;
	curserLoc = 0;
}else if(curserLoc == 1 && keyboard_check_pressed(vk_enter)){
	global.singlePlayer = false;
	currentState = menuState.preGame;
	curserLoc = 0;
}else if((curserLoc == 2 && keyboard_check_pressed(vk_enter)) || keyboard_check_pressed(vk_escape)){
	game_end();
}