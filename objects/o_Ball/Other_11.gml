/// @description The Game Is Running

//Check for collision with roof or floor
if(y + yy * moveDirY < 1 + sprite_height/2){
	while(y > 1 + sprite_height/2){
		y -= 1;	
	}
	moveDirY *= -1;
}else if(y + yy * moveDirY > room_height - 1 - sprite_height/2){
	while(y < room_height - 1 - sprite_height/2){
		y += 1;	
	}
	moveDirY *= -1;
}

//Check if the ball was scored
if(x < 0){
	moveDirX = 0;
	moveDirY = 0;
	global.player2score += 1;
	global.ballScored = true;
	x = 0;
	image_alpha = 0;
	audio_play_sound(s_PointScored, 1, false);
}else if(x > room_width){
	moveDirX = 0;
	moveDirY = 0;
	global.player1score += 1;
	global.ballScored = true;
	x = room_width;
	image_alpha = 0;
	audio_play_sound(s_PointScored, 1, false);
}

//Check for collision with paddles
if(place_meeting(x + xx * moveDirX, y, o_Player1)){
	audio_play_sound(s_PaddleHit, 1, false);
	while(!place_meeting(x + 1, y, o_Player1)){
		x -= 1;	
	}
	if(ballSpeed < maxSpeed){
		ballSpeed += speedUp;
		xx = lengthdir_x(ballSpeed, moveAngl);
		yy = lengthdir_y(ballSpeed, moveAngl);
	}
	moveDirX *= -1;
}else if(place_meeting(x + xx * moveDirX, y, o_Player2)){
	audio_play_sound(s_PaddleHit, 1, false);
	while(!place_meeting(x - 1, y, o_Player2)){
		x += 1;	
	}
	if(ballSpeed < maxSpeed){
		ballSpeed += speedUp;
		xx = lengthdir_x(ballSpeed, moveAngl);
		yy = lengthdir_y(ballSpeed, moveAngl);
	}
	moveDirX *= -1;
}

//Moving the ball
x += xx * moveDirX;
y += yy * moveDirY;




