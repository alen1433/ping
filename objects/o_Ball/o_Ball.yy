{
    "id": "5ab012a2-bc7f-4ae8-8275-e689217e5e69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_Ball",
    "eventList": [
        {
            "id": "afe1ef2d-e48b-4654-9176-1c550d663e9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ab012a2-bc7f-4ae8-8275-e689217e5e69"
        },
        {
            "id": "9e219c87-98be-4998-a959-a44011be0083",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ab012a2-bc7f-4ae8-8275-e689217e5e69"
        },
        {
            "id": "be6827ad-f719-4cee-bae9-b7f9bef11c71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5ab012a2-bc7f-4ae8-8275-e689217e5e69"
        },
        {
            "id": "9854c724-0249-40b5-b229-0538711bde6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5ab012a2-bc7f-4ae8-8275-e689217e5e69"
        },
        {
            "id": "a3f2b130-12af-49a6-89d4-9be639bdde30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "5ab012a2-bc7f-4ae8-8275-e689217e5e69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73dfe204-c65d-453b-8354-d3e1f412ff7d",
    "visible": true
}