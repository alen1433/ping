/// @description The Initalization script
image_xscale = 1.5; image_yscale = 1.5;
depth = -1;

ballSpeed = 5.0;
speedUp = 1;
maxSpeed = 20.0;
randomise();

switch(global.theme){
	case 0: sprite_index = BallBasic; break;
	case 1: sprite_index = BallInverted; break;
	case 2: sprite_index = BallSoccer; break;
	case 3: sprite_index = BallBeach; break;
}