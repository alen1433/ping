/// @description A point was scored

//Starting position
x = room_width / 2;
y = room_height / 2;
ballSpeed = 5.0;
image_alpha = 1.0;

//Choosing a random Direction
moveDirX = irandom_range(-1, 0);
moveDirY = 1;
if(moveDirX == 0){ moveDirX = 1; }

//Randomising the angle
moveAngl = irandom_range(125,235);
if(165 < moveAngl < 195){
	moveAngl = 160;
}
xx = lengthdir_x(ballSpeed, moveAngl);
yy = lengthdir_y(ballSpeed, moveAngl);