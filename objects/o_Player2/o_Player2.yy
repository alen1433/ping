{
    "id": "784541e6-95b3-49c9-a346-7db3a87d3582",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_Player2",
    "eventList": [
        {
            "id": "6c6928e0-c055-44e1-873c-2955074dd207",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "784541e6-95b3-49c9-a346-7db3a87d3582"
        },
        {
            "id": "adf1067b-f4c6-4967-8c65-8c3e4c32f049",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "784541e6-95b3-49c9-a346-7db3a87d3582"
        },
        {
            "id": "d2982138-a4a9-4325-a0a9-59d0692e4ed5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "784541e6-95b3-49c9-a346-7db3a87d3582"
        },
        {
            "id": "7d4eb702-e637-414b-bd7c-5f654a6b7512",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "784541e6-95b3-49c9-a346-7db3a87d3582"
        },
        {
            "id": "449f7c64-9563-44b3-a4f3-bb19f85116a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "784541e6-95b3-49c9-a346-7db3a87d3582"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b51dc4cb-58fc-442b-97f1-bee3864b11e9",
    "visible": true
}