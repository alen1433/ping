/// @description The Game Is Running

//set direction of movement
if(global.singlePlayer){
	if(y > o_Ball.y){
		moveDir = -1;
	}else if(y < o_Ball.y){
		moveDir = 1;	
	}
}else{
	moveDir = 0 - keyboard_check(vk_up) + keyboard_check(vk_down);
}

//check for collision with roof and floor
if(y + global.paddleSpeed * moveDir < 1 + sprite_height/2){
	while(y > 1 + sprite_height/2){
		y -= 1;	
	}
	moveDir = 0;
}else if(y + global.paddleSpeed * moveDir > room_height - 1 - sprite_height/2){
	while(y < room_height - 1 - sprite_height/2){
		y += 1;	
	}
	moveDir = 0;
}

//finally move paddle
y += global.paddleSpeed * moveDir;
