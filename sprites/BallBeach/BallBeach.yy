{
    "id": "250988fc-5f92-4d25-aa0a-3d4ba6756c12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BallBeach",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c62ce055-e758-4f09-940b-34938343f0fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "250988fc-5f92-4d25-aa0a-3d4ba6756c12",
            "compositeImage": {
                "id": "51bc11a7-3c62-4c9a-91f5-b5905e3e2fc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c62ce055-e758-4f09-940b-34938343f0fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8355b04-9ec6-45be-a0d4-f653f7b3ee54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c62ce055-e758-4f09-940b-34938343f0fa",
                    "LayerId": "9d9d7b2d-aef4-42b1-94d8-ddcc470a0937"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "9d9d7b2d-aef4-42b1-94d8-ddcc470a0937",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "250988fc-5f92-4d25-aa0a-3d4ba6756c12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}