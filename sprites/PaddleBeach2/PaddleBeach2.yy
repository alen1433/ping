{
    "id": "106bbc4e-0611-440f-b866-d4ce15b4067e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PaddleBeach2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f69de5b-39ea-4e71-88a1-c73c7d515929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "106bbc4e-0611-440f-b866-d4ce15b4067e",
            "compositeImage": {
                "id": "1dbc0cb2-4d55-4ec6-949f-80700a854434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f69de5b-39ea-4e71-88a1-c73c7d515929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e29704-88da-4fd5-aa93-d056ad16b124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f69de5b-39ea-4e71-88a1-c73c7d515929",
                    "LayerId": "a8a26d9e-49db-4f74-b819-a074d5bb115d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a8a26d9e-49db-4f74-b819-a074d5bb115d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "106bbc4e-0611-440f-b866-d4ce15b4067e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}