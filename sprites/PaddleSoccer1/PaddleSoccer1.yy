{
    "id": "90406741-8229-412a-9f01-e7d9dfe6bb40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PaddleSoccer1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f12dc66-ca8c-4c1f-b71d-da93e77725ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90406741-8229-412a-9f01-e7d9dfe6bb40",
            "compositeImage": {
                "id": "f6cb4fe3-b7ec-40d0-9229-8462ebfb2dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f12dc66-ca8c-4c1f-b71d-da93e77725ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad41083f-39f3-4713-9b1b-08ccced45203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f12dc66-ca8c-4c1f-b71d-da93e77725ed",
                    "LayerId": "1de862c6-67a0-4bb1-b6c9-86e4f1edef89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1de862c6-67a0-4bb1-b6c9-86e4f1edef89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90406741-8229-412a-9f01-e7d9dfe6bb40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}