{
    "id": "dad68160-3631-460a-8cd4-ecfd42d97803",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PaddleSoccer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25a1e2fb-d499-4f8e-9295-4349e302f449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dad68160-3631-460a-8cd4-ecfd42d97803",
            "compositeImage": {
                "id": "dc3da802-9d97-4343-95c1-fe7f8e7665f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a1e2fb-d499-4f8e-9295-4349e302f449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1840d5fb-7f95-4050-8e21-d6f737d046c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a1e2fb-d499-4f8e-9295-4349e302f449",
                    "LayerId": "95a7e1d2-7e49-4dae-b539-6a3416addb1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "95a7e1d2-7e49-4dae-b539-6a3416addb1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dad68160-3631-460a-8cd4-ecfd42d97803",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}