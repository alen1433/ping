{
    "id": "6162a468-96b4-410b-9129-024e3570d152",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BgSoccer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c0a7510-ebda-41b1-836c-d926db85a346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6162a468-96b4-410b-9129-024e3570d152",
            "compositeImage": {
                "id": "7674cfc4-9d88-4a5e-959e-35c3074590f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c0a7510-ebda-41b1-836c-d926db85a346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fce420eb-2dea-4e8a-9320-f4f148161fda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c0a7510-ebda-41b1-836c-d926db85a346",
                    "LayerId": "5a433aa9-5b05-4750-ac1a-f3e92263a303"
                }
            ]
        }
    ],
    "gridX": 256,
    "gridY": 256,
    "height": 768,
    "layers": [
        {
            "id": "5a433aa9-5b05-4750-ac1a-f3e92263a303",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6162a468-96b4-410b-9129-024e3570d152",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 384
}