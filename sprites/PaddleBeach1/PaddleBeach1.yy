{
    "id": "ca41aade-00a4-4e7b-9c9c-a63858c7665b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PaddleBeach1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4712c335-9850-496e-b81d-e196dc51f6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca41aade-00a4-4e7b-9c9c-a63858c7665b",
            "compositeImage": {
                "id": "f8fff744-d0d1-4d90-8785-01e5d0b70a69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4712c335-9850-496e-b81d-e196dc51f6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a48d29-b3c2-4657-90f1-d321db8acf43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4712c335-9850-496e-b81d-e196dc51f6a0",
                    "LayerId": "1a34489b-62e9-45b6-8c4c-30abc5f675b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1a34489b-62e9-45b6-8c4c-30abc5f675b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca41aade-00a4-4e7b-9c9c-a63858c7665b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}