{
    "id": "d8247c52-8fc9-436e-a27e-78614475473d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BgBeach",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "973e631a-b7d9-4e7d-ac81-5be6e7b9a42e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8247c52-8fc9-436e-a27e-78614475473d",
            "compositeImage": {
                "id": "744124f8-cb7a-40a6-8282-c048c702ddf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "973e631a-b7d9-4e7d-ac81-5be6e7b9a42e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c31a2554-ec42-4e78-883a-120396a50db7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "973e631a-b7d9-4e7d-ac81-5be6e7b9a42e",
                    "LayerId": "bb7d7e04-3fe0-45b9-b43e-2363e6dc4396"
                }
            ]
        }
    ],
    "gridX": 256,
    "gridY": 256,
    "height": 768,
    "layers": [
        {
            "id": "bb7d7e04-3fe0-45b9-b43e-2363e6dc4396",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8247c52-8fc9-436e-a27e-78614475473d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 384
}