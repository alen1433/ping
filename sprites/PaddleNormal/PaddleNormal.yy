{
    "id": "b51dc4cb-58fc-442b-97f1-bee3864b11e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PaddleNormal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 37,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2784f88-d854-4ff3-8fa4-0e10f464b033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b51dc4cb-58fc-442b-97f1-bee3864b11e9",
            "compositeImage": {
                "id": "b26393d0-ebaf-4238-8d84-dbf53b3840e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2784f88-d854-4ff3-8fa4-0e10f464b033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9062bb34-7502-4cd1-8704-b4a191575c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2784f88-d854-4ff3-8fa4-0e10f464b033",
                    "LayerId": "b0f05ed0-9393-4f8d-9f3d-626251604088"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b0f05ed0-9393-4f8d-9f3d-626251604088",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b51dc4cb-58fc-442b-97f1-bee3864b11e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}