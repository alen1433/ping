{
    "id": "5d25b107-aa17-4caf-95d3-a0526f0abf76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PaddleInverted",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 26,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "186d3c50-fd0c-4465-827d-87d0180451b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d25b107-aa17-4caf-95d3-a0526f0abf76",
            "compositeImage": {
                "id": "a0a4ca59-940e-4c07-86f1-a8b2894d69cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "186d3c50-fd0c-4465-827d-87d0180451b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a4e2aaf-0444-4bbd-9183-919efd884c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "186d3c50-fd0c-4465-827d-87d0180451b7",
                    "LayerId": "89df57fc-a20d-4b24-baa8-81de6d77777b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "89df57fc-a20d-4b24-baa8-81de6d77777b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d25b107-aa17-4caf-95d3-a0526f0abf76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}