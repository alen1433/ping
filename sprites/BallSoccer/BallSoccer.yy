{
    "id": "510ae7da-fbfa-4e4d-9ec8-25425465d5e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BallSoccer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0af6f6d1-b5b4-46f1-85b8-e919aa5aa681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "510ae7da-fbfa-4e4d-9ec8-25425465d5e6",
            "compositeImage": {
                "id": "df496083-1c8f-4b11-acd2-296c47a2918d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0af6f6d1-b5b4-46f1-85b8-e919aa5aa681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d7ac8e-7007-4b4b-9118-6fdf6c0d64db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0af6f6d1-b5b4-46f1-85b8-e919aa5aa681",
                    "LayerId": "f5f46c99-de04-490b-8a55-316285fcb02a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "f5f46c99-de04-490b-8a55-316285fcb02a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "510ae7da-fbfa-4e4d-9ec8-25425465d5e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}