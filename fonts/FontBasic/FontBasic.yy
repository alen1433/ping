{
    "id": "5fba3d87-dfd3-4756-b560-ba2f60e3b85c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "FontBasic",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "square deal",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4e6237fc-9a3e-4585-9380-cb326b5865c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "07e5a248-64dd-42cc-9b80-0b9d83d9ebf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 56,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a47ca849-c1f0-4b69-9587-894f0b0ef859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 48,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fd1e672a-1489-4747-81ea-bd8c4e5fad50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 36,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "20adf695-cf1e-4e08-9cba-174abf40573f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 26,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "612e701a-faf4-4af6-be49-4ffc74aa90b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5fc5afd7-beea-471f-bb34-b565459945de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7426f5d6-3b5e-4354-8f71-e7642ae7c9d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 243,
                "y": 21
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7f352ba9-d139-4c1c-a99f-d4ca3f4310c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 235,
                "y": 21
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "72b33357-bc9c-4356-ab14-cf76accf511e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 227,
                "y": 21
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "eb289c0c-c581-49ac-a8c4-9964d9ea1bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 60,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2e23c707-c969-4944-bb48-175b4fd49f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 217,
                "y": 21
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6a3a3f09-e7cb-488b-89a7-c3ff666051c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 205,
                "y": 21
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "13743b20-694e-4a77-aa77-5a0de16288a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 197,
                "y": 21
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4b06b47c-8432-4d49-930f-85b3306d018b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 193,
                "y": 21
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "217a843c-2ca3-4046-a7e4-08bd766d391b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 184,
                "y": 21
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3a4bfcd0-867c-482f-bbf1-29d8dcf95bc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 176,
                "y": 21
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "50b3f28d-fee1-4e7d-a031-238249197417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 172,
                "y": 21
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "06f4db7d-1832-4533-b726-dace0b64cb88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 164,
                "y": 21
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5a0e6827-166d-4bbc-9e07-5a188270c1fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 156,
                "y": 21
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e046875e-7328-4809-af90-851a5f968bc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 148,
                "y": 21
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "672e72d1-3317-4adf-89a0-db434c945f9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 209,
                "y": 21
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c0f42429-fda4-4f82-9e0d-7070867a2e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 70,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "47510232-262e-40a8-88e1-12ed7dedd2aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9e498889-378f-4268-8fd4-005b47abf63a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 86,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c411e5a1-2c48-4b88-9fe5-f410477da707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7b57ba01-f3ff-4bfd-b978-9af1352f5730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 245,
                "y": 40
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "794915b8-ae70-4e1f-b2c9-f2db5fb96a9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 240,
                "y": 40
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1c27a748-a5d0-4fb4-ad5c-a6f9fe8957c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 230,
                "y": 40
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "629eb2e9-6337-4323-9159-704ebe6ec230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 220,
                "y": 40
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9629f9c1-e345-4aa1-8c36-170af938790a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 40
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "977e71a0-f998-4408-9af7-aa2950e66816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 202,
                "y": 40
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "acfb246a-f1e8-44ac-b0fb-b353aba562e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 40
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "50c5cf86-d533-48d2-960d-71b9357f6d81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 182,
                "y": 40
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3af900d1-a486-45e6-8f93-b864124d3d6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 174,
                "y": 40
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8684bc44-52bd-4ee9-b7de-e856d74630b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 166,
                "y": 40
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cb6b35f3-189f-45e2-abfc-80cd7b1e9c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 158,
                "y": 40
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "26ca9e10-f322-4eba-ac78-59724b0f7207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 150,
                "y": 40
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b6ddea95-2620-4752-8362-646253a77e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 144,
                "y": 40
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "eee9b970-40ef-4c9d-8509-51ed755daae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 136,
                "y": 40
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "14065705-1eae-42b2-8c08-655f2c7fdbcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 128,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d978f763-4ce8-4daf-aef1-34c30b46e736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 124,
                "y": 40
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "eec655b0-7fba-4d88-bb3d-bde8937c8bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 118,
                "y": 40
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "463e71f0-3483-442d-b931-e9532321de74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 110,
                "y": 40
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "72a44bfb-1621-4d2e-955e-31da5ea685b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 106,
                "y": 40
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d33cba10-d974-41a0-93ca-50bbb1dc27ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 94,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "aa7bcfb0-0df0-47f9-87ce-9d6cd1587dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 140,
                "y": 21
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "57291ec4-b96f-43e1-922a-42239f3c5a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 132,
                "y": 21
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "debed985-e7c4-41a4-89c6-84021deda6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 124,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5ba7986a-97bc-4bac-8cd8-27fa67c5c8a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3a2ebb3d-743f-4f43-a671-9bf2e83f918f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c69269c0-7c02-4cef-a118-b733409cb209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b9d22cb3-ad29-4061-a2c9-6a5cdf982526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6523c600-9e14-4993-a180-58debcbb7234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4a59a882-efda-4360-b883-7f0a98eb193a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6dfd3b8f-d0be-452b-b1a9-89f914504b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "349fd14b-f1ce-4062-ac29-45e24fcc5747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "690755d0-d97f-46b0-a77b-8acc6e484f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "74739116-27f3-4263-8319-7c7430860368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "515e68e9-bd5d-4c86-be83-86fb6231f0f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "df5ab0fd-8976-4ca5-92a6-80145d38794b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "03ccdec8-6df9-4a48-9bf4-9140bfa6e6d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1524b664-3a9e-4839-9747-ef452b488970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "52520e49-db1c-41f0-ae0b-af8a6c5d607a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c4e91dd8-0edd-4531-9432-74c6dc9a92b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "57e029a6-7e19-4653-8a84-0e606824e38f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "49c70646-d3e9-45c5-8c90-356cd897bdf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8b91a0bd-6be3-4621-ae8d-645f8b7f3148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4d53fcbe-4b19-49e3-9387-626a9ba1d71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5da3ca9a-2755-48a2-b922-e075f289ef75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "353366ac-a582-43f2-b200-72d3fca3371f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "356c06b7-3341-43be-9e6c-9271a945a265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b4c1943a-4bb4-4a00-9704-9f927a46b351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 30,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "67eb2308-6e37-4d9a-b780-6804df1cc0d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "51eef2c6-ea09-450e-ba4b-08e926111eca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 110,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "fefbbe04-6f34-490e-93a6-1f5383b5fb00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 102,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fe8b694f-5ef5-4725-a8e5-fce1582e7701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 98,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5617f127-66fd-4579-9a6a-7515d73d06f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c1e18d5f-6eb6-4484-a246-6afd696df393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1d7bcaee-5cdf-47d0-8d4a-ee0937da9e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 70,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c6f18c40-99c0-4a1a-8ca1-98bcc129fc08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 62,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "86d051ef-fd68-4cc6-be18-2240aff764f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 54,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "33b30b55-b57c-4d2f-81e6-245046b2f58d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ba460ab9-54cd-4a1b-b0b2-910ecc047293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 116,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "49ef4216-649c-4d89-9e12-a97d684e04b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 38,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "403fa4c2-bcfc-46d4-9fb0-79d9cedfd52b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 22,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "dc1f303c-4f08-4ea6-84e8-699fa9ab90ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 14,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e94ef6f5-5cb8-4954-abc5-2b76107af430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "656e465d-dd34-47e0-9a74-5c3b651dd68e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "baa14cce-70b4-4c9c-a886-327e495f142a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "23e2cf91-c856-49a1-a0f6-279107b3ad3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "930355cb-c1df-4380-911a-fd55022788f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e9cf5bba-98cc-4622-8953-d18a2b2a4a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d53763ff-f7fc-4a63-8ded-b03d6dc47d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "183f3554-e89e-41f2-9359-3604fff3fa79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 10,
                "y": 59
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c62654cb-b3bd-4bc4-905c-f69f303507da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 20,
                "y": 59
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}