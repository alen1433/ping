{
    "id": "5fba3d87-dfd3-4756-b560-ba2f60e3b85c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "FontBasic",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "square deal",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2628206d-a650-4f29-99ee-217d9b32f09f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "194f9182-8243-4fe1-a4cf-3b05b32146e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 56,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6682cc03-1056-4307-9f50-665d44b7f472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 48,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "071dbac6-404f-4013-823c-5c94c800c520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 36,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "12429154-0233-43b2-8759-99ef92259eee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 26,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ab384379-999e-4a25-9d06-a6d8bb2a76c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5812aef9-a3cd-4ea9-8f51-c69300a4a829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4f4dedc7-2f9c-41a5-a10a-6742daf7a197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 243,
                "y": 21
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2c364764-2997-4b6c-9efe-fc476c7c9474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 235,
                "y": 21
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cee549e4-a7f5-4ae6-8645-135eb02c4d43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 227,
                "y": 21
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8f23c7aa-9f21-40b6-b47f-d6126d0b5323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 60,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b6169fd3-ec09-4af4-8c70-30d0d24a466d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 217,
                "y": 21
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bac2a604-c1bb-4116-a280-3b368de88a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 205,
                "y": 21
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b4c1ad42-2127-4470-b613-931ab48cb067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 197,
                "y": 21
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "063a80db-caa6-4d92-b987-75ba7ccb1db3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 193,
                "y": 21
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "36140f6c-20a8-4b2a-b332-d56b80250699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 184,
                "y": 21
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "543fb588-f2e1-485b-b4b0-4237215e851d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 176,
                "y": 21
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "168f8e22-9131-4218-bff5-f6f3c457ebd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 172,
                "y": 21
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a160fc07-2d4b-4006-b272-9b101ee34a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 164,
                "y": 21
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a60b7659-9b5a-48c8-9349-537ea28f3348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 156,
                "y": 21
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "28cdb2a9-a193-4fb4-92f0-6f74a9624796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 148,
                "y": 21
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "57a57e93-0262-4f18-aa80-0329849fbbdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 209,
                "y": 21
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c12eba48-ab2b-4f94-91f7-0d8458f5d126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 70,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f1a762c5-e983-4c3d-bc6d-e1472e29f4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "49c5a264-e51c-4da3-8908-7a71fa9a5b26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 86,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b957de6d-e5c8-4eb1-a372-8c038b87bea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2bd49a73-2fab-4a91-8f90-f432f5f33bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 245,
                "y": 40
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "35d36479-7c37-4db9-82d6-6d55916dfe31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 240,
                "y": 40
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d5b09467-2714-46c5-abdf-dc93129bba2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 230,
                "y": 40
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9ae261d7-0ac6-4a71-82ab-831879ac00e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 220,
                "y": 40
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e8928618-3876-42b8-83a8-fa2051191403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 40
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "471d4cce-f514-484c-a6b9-1b6300b74c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 202,
                "y": 40
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a0979cc4-c977-4ad8-a1bf-8e9c6d77b2b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 40
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0c125176-4ba7-42a1-83e7-1bae342993c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 182,
                "y": 40
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "18d6f1cd-9664-415d-9c5f-56e8f05f9d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 174,
                "y": 40
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "42850d46-c5c7-4cb3-a17b-a2a6d8a78ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 166,
                "y": 40
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1dd0477d-8825-4ddf-9cf9-4c2896e9e6d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 158,
                "y": 40
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "029a3472-fdec-418a-88eb-e4f0e27a6eb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 150,
                "y": 40
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d4cac569-993a-4c07-b7b0-0cff90f90635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 144,
                "y": 40
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "00e7f76f-be4a-479f-8cd8-785b70efccdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 136,
                "y": 40
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e495d1a8-7d99-4f2b-ba1b-8cd763230594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 128,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ca31c792-722b-4670-b456-aa965e46db48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 124,
                "y": 40
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "46c80331-0634-44f9-9a7b-5ccba2450943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 118,
                "y": 40
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5e1d9267-6c77-4ddb-a4c6-f77be11df73e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 110,
                "y": 40
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "16920803-056b-4aee-b257-e2c14a2dd15b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 106,
                "y": 40
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6a56eaa5-f1c3-42a9-9323-c3bd4af4052e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 94,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "16e32718-faae-4b36-bf1d-3ca452dcb301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 140,
                "y": 21
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "09fcf7cd-02b3-4671-9321-b960c802cfc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 132,
                "y": 21
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5e1e71df-dbda-4a19-918d-63e58a438efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 124,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "343dff9e-c91c-4ccc-a06a-9f1fe5e0aa0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1e736201-81f7-4bcc-9f81-26b12a35e318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d38792c7-137d-42cc-bc2b-35e55995cf35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d34f1900-309a-4e24-9609-f55fb4ef9104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8ab6696e-3c52-42fe-bac4-f967142e5ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4a5fb74d-8540-4a5a-8c7f-b6d96e2405dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8fc671cc-2308-489e-8e8f-13b7b2b014bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b6082a24-79c3-46dc-9326-be8ce3d62c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "29b1144e-56b5-487e-bc5c-7d332fa0f190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "bf0f64fe-8cb7-4b82-8e7d-12721f0c9ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "40fb306e-0b9f-4efa-ab3a-255cc1ecbeea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "16e4d5c5-f9d2-4ef2-bfe7-db883abd45db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a410c9ad-bb5c-47c8-9f35-41910c814b54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "28f0da41-6f71-4f37-8216-39f178175b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9d98be9d-e1c1-49bf-a169-cbc285bf2596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a954c3e5-e6c2-4907-abee-13837407e42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "786115f0-4f99-4e3d-90a7-ad8b8dd887f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "96dc9154-8571-4dd1-aa35-3800ca503b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8e4ce18a-3b78-455c-a7fa-b780bcd16a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "bea7f596-11ec-4010-96c7-c5c2713d85e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b84c4298-5757-4812-aabf-89c1f94656ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7a056037-45f0-44c9-80db-8a3e4f392917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0ddc43b2-b76f-42d5-aa95-3ba5c5d74b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e28600a1-824c-4601-b77a-3fc89e5fb384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 30,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1a9e1abe-e9d7-43e7-ab7f-f082e74e7795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f039e414-ba68-4a88-bb7a-291febff3d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 110,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ba3dd15e-9db4-4645-a62f-fd93e3283a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 102,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "cbe59c07-0cba-46b2-869b-751da8e649a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 98,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "26b103d8-c95d-4bab-b453-699860a5b68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c30b68e9-14cf-48d1-9683-67dd1326fe6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1aacc2d1-76ba-4d0f-9f08-6da9aeff4811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 70,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "228986df-1476-4ffd-a271-90388715e989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 62,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7f46eb8f-27d4-4297-8a9f-dce474ef9336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 54,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "899f2b63-6f94-4bec-ad7f-0abfaaa1d297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ce70c02a-0785-4e3f-a795-eab3bd69ef08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 116,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9c447731-5b16-472e-977c-40911791e137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 38,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "632ce1ad-6738-451d-90fd-6670c259e2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 22,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6dae4b7d-bf7d-4e32-b489-dda5b68be8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 14,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d9e0a7d9-958c-41ab-8475-ebccc0b3e170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "033ac846-2983-4537-84cb-0a5c88770db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9b82d1e4-6161-4652-a924-0f395cbae52e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e219c137-b7d7-4436-96cf-412d14327c79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "372cdf28-0a89-40df-9502-15724da5d6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8e7149d5-23ac-4045-98b7-4476c680c0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "edd3b4a4-9c9f-4b4b-979f-8007680e4cbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5036154d-8a6e-4e05-87b6-b6fdb1f29cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 10,
                "y": 59
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "393e7087-e474-4b75-b010-9c5531c78638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 20,
                "y": 59
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}